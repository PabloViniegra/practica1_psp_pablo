package controller;

import views.View;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseManagement extends Thread {
    public final String ERROR_PROPERTIES = "Falta el archivo properties de la Base de Datos";
    public final String EXIT_QUERY = "Consulta realizada con éxito";
    public final String ERROR_QUERY = "Error en la consulta";
    private int initial;
    private int last;

    public DatabaseManagement (int initial, int last) {
        this.initial = initial;
        this.last = last;
    }

    //return the chain of connection
    public Connection returnConnection() {
        File file = new File("bbdd.properties");
        Properties properties = new Properties();
        String db;
        String host;
        String url;
        String user;
        String password;
        Connection conn = null;
        if (!properties.isEmpty())
            System.out.println(ERROR_PROPERTIES);

        try (FileInputStream fis = new FileInputStream(file)) {
            properties.load(fis);
            db = properties.getProperty("database");
            host = properties.getProperty("dbhost");
            user = properties.getProperty("dbuser");
            password = properties.getProperty("dbpassword");
            url = "jdbc:mysql://" + host + "/" + db;

            conn = DriverManager.getConnection(url, user, password);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
            System.err.println(e.getLocalizedMessage());
            View.menu();
        }
        return conn;
    }

    public synchronized void insertDataToDatabase() {
        try (Connection conn = returnConnection()) {
            String query;
            for (int i = this.initial; i <= this.last; i++) {
                query = "INSERT INTO EMPLEADOS (EMAIL,INGRESOS) VALUES (?,?)";
                PreparedStatement statement = conn.prepareStatement(query);
                statement.setString(1, Home.generateRandomEmails());
                statement.setInt(2, Home.generateRandomSalary());
                int value = statement.executeUpdate();

                if (value == 1)
                    System.out.println(EXIT_QUERY + " " + getName());
                else
                    System.err.println(ERROR_QUERY);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            System.err.println(throwables.getLocalizedMessage());
            View.menu();
        }
    }

    public void run() {
        insertDataToDatabase();
    }


}
