package controller;

import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import views.View;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Home {
    public static void main(String[] args) {
        View view = new View();
        Scanner sc = new Scanner(System.in);
        DatabaseManagement data;

        View.menu();
        int registers = view.requestDataUserRegisters(sc);
        int threads = view.requestDataUserThreads(sc);
        try {
            double workToDo = registers/threads;
        } catch (ArithmeticException e) {
            System.err.println("Imposible dividir entre cero");
        }
        //For loop to make each thread take care of a part of the work.
        int initial, last;
        for (int i = 0; i < threads; i++) {
            initial = (registers / threads) * (i - 1);
            if (i == threads)
                last = registers;
            else
                last = (registers/threads) * i;

            if (i == 1)
                initial = 1;
            else
                last = (registers/threads) * i;
            data = new DatabaseManagement(initial, last);
            data.start();


        }
        System.out.println("Datos insertados con éxito");
    }
    //With JavaFaker we can generate random emails, check the pom.xml
    public static String generateRandomEmails () {
        FakeValuesService fake = new FakeValuesService(new Locale("es"), new RandomService());
        String email = fake.bothify("????##@gmail.com");
        Matcher emailMatcher = Pattern.compile("\\w{4}\\d{2}@gmail.com").matcher(email);

        return  email;
    }

    public static int generateRandomSalary () {
        return (int) Math.floor(Math.random() * 10000 + 10);
    }
}
