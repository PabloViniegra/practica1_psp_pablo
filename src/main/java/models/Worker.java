package models;

public class Worker {
    private int id;
    private String email;
    private int salario;

    public Worker () {

    }

    public Worker (int id, String email, int salario) {
        this.id = id;
        this.email = email;
        this.salario = salario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }
}
