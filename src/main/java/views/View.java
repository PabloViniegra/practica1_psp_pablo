package views;

import java.util.InputMismatchException;
import java.util.Scanner;

public class View {

    public static void menu() {
        System.out.println("Primero indique el numero de registros a insertar.");
        System.out.println("Luego indique el número de hilos para trabajar.");
    }

    public int requestDataUserRegisters (Scanner sc) {
        System.out.print("Indique el número de registros: ");
        int registers = 0;
        boolean check = false;
        while (!check) {
            try {
                registers = sc.nextInt();
                check = true;
            } catch (InputMismatchException e) {
                System.out.print("Por favor, introduzca un valor numérico: ");
                sc.next();
            }
        }
        return registers;

    }

    public int requestDataUserThreads (Scanner sc) {
        System.out.print("Indique el numero de hilos: ");
        int threads = 0;
        boolean check = false;
        while (!check) {
            try {
                threads = sc.nextInt();
                check = true;
            } catch (InputMismatchException e) {
                System.out.print("Por favor, introduzca un valor numérico: ");
                sc.next();
            }
        }
        return threads;
    }

}
