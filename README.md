# GUÍA DE LA APLICACIÓN

### ENUNCIADO DEL EJERCICIO

Aplicación 1:
1.1 A través de consola, solicita al usuario un número de registros a insertar.
1.2 A través de consola, solicita al usuario un número de hilos.
1.3 Inserta el número de registros introducido en una base de datos utilizando el número de hilos indicado. Los datos se generarán de forma aleatoria, debiendo estar los ingresos comprendidos entre 10 y 1000.

### REPOSITORIO
GitLab
[link de repositorio](https://gitlab.com/PabloViniegra/practica1_psp_pablo.git)

### TECNOLOGÍA EMPLEADA
- **Lenguaje de programación:** Java
- **Version:** 8
- **JDK:** 1.8.0_251-b08
- **Entorno de Desarrollo:** IntelliJ IDEA ultimate version
- **Estándar:** Proyecto Maven

### ¿EN QUÉ CONSISTE?

La aplicación  está construida de forma secuencial hasta la llamada de los hilos. El usuario elige el **número de registros** que quiere insertar y los **hilos** con los que quiere trabajar. Una vez hecho esto, comienza la aplicación; cada uno de los hilos se divide el trabajo insertando un número de registros (en caso de no ser exactos, los registros sobrantes se repartirían entre los hilos que accedan).

La conexión a la Base de Datos se ha realizado a través de un `bbdd.properties`que se encuentra en la raíz del proyecto. Dicho fichero contiene la URL, el usuario y el password que construirán la cadena de conexión. Los registros se insertan de forma aleatoria, incluso el email que sigue una expresión regular. Para trabajar con la generación de emails falsos se ha empleado la dependencia de **JavaFaker**.

```
<dependency>
        <groupId>com.github.javafaker</groupId>
        <artifactId>javafaker</artifactId>
        <version>1.0.2</version>
</dependency>
```

### REQUISITOS PARA INICIAR LA APLICACIÓN

La aplicación está comprimida en un `.jar`. Para acceder a ella necesitamos seguir los siguientes pasos:

- Primeramente, necesitaremos alojar en nuestro ordenador la Base de Datos. Pueden usarse distintos paquetes como XAMPP, WAMPP ..etc.
- Después, necesitaremos crear la Base de Datos. Los datos son los siguientes:
  - Nombre de la Base de Datos: BBDD_PSP_1
  - Tabla: EMPLEADOS
  - Host: Localhost
  - Usuario: DAM2020_PSP
  - Contraseña: DAM2020_PSP
    - ID: PK. Integer. Autoincremental
    - EMAIL: varchar(100)
    - INGRESOS: Integer
- En su defecto, puedes usar de usuario `root` y contraseña vacía.
- Puedes fijarte en el archivo `bbdd.properties` para los datos.

### INICIANDO LA APLICACIÓN

Debemos acceder al `.jar` con un terminal de consola. Cualquiera puede usarse. Si usted usa Windows como SO puede pulsar **tecla Windows** + **R** y escribir `cmd` para abrir el terminal de Windows. Escribiendo `cd` y la ruta de directorio puede moverse hasta donde tiene la carpeta del proyecto. Otra opción es copiar la ruta donde se encuentra el proyecto y escribir `cd [ruta del portapapeles]`. Una vez situado allí, escriba en la terminal: `java -jar nombredeljar.jar`, en esta caso escriba el nombre del .jar que hay en la raíz del proyecto. Con esto, debería ejecutarse la aplicación en su consola.